This sample application is called **Trip summary**, it aims to track all the history of customer trips.
**Requirements:**
- Java 8
- Maven (build tool)

**Execution:**
The main class of this application is TripSummaryApplication. You need to execute its main method
in order to execute the application.
You'll be asked to enter two variables :
1- First input = design the path of the input file that we want to process (ex = C:\opt\input.txt)
2- Second input = design the desired path where we want to save the generated file after the process operation (ex = C:\opt)