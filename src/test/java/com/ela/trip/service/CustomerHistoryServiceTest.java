package com.ela.trip.service;

import com.ela.trip.domain.Tap;
import com.ela.trip.domain.Trip;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class CustomerHistoryServiceTest {

    private CustomerHistoryService service = new CustomerHistoryService();

    @Test
    public void testGroupTapByCustomer() {
        List<Tap> taps = Arrays.asList(new Tap(1572242400L, 1, "A"), new Tap(1572244200L, 2, "D"));
        Assert.assertEquals(2, service.groupTapByCustomer(taps).size());
    }


    @Test
    public void testgetKeys() {
        String value = "C";
        Assert.assertTrue(service.getKeys(value).size() == 2);
    }

    @Test
    public void testGetLowCostTrip() {
        List<Trip> trips = Arrays.asList(new Trip(1, 2, 200), new Trip(1, 4, 280));
        Assert.assertNotNull(service.getLowCostTrip(trips));

    }


}
