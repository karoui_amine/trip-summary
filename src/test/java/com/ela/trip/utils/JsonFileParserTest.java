package com.ela.trip.utils;

import org.junit.Assert;
import org.junit.Test;


public class JsonFileParserTest {

    private static final String FILE_PATH = "src/test/resources/input-file.txt";


    @Test(expected = NullPointerException.class)
    public void parseInputFileShouldThrowException() throws Exception {

        JsonFileParser.parseInputFile("");

    }

    @Test
    public void parseInputFileShouldBeFine() throws Exception {

        Assert.assertNotNull(JsonFileParser.parseInputFile(FILE_PATH).isPresent());

    }

    @Test(expected = NullPointerException.class)
    public void parseOuputFileShouldThrowException() throws Exception {

        JsonFileParser.generateOutputFile(null, null);

    }
}
