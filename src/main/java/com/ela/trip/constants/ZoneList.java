package com.ela.trip.constants;

import java.util.*;

public class ZoneList {

    public static final Map<Integer, List<String>> STATIONS_BY_ZONE = initialiseZoneStation();

    public static Map<Integer, List<String>> initialiseZoneStation() {

        Map<Integer, List<String>> map = new HashMap();
        map.put(1, Arrays.asList("A", "B"));
        map.put(2, Arrays.asList("C", "D", "E"));
        map.put(3, Arrays.asList("C", "E", "F"));
        map.put(4, Arrays.asList("F", "G", "H", "I"));
        return Collections.unmodifiableMap(map);

    }
}
