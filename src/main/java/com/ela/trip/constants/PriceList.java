package com.ela.trip.constants;

import com.ela.trip.domain.TravelZone;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PriceList {

    public static final Map<TravelZone, Integer> PRICE_BY_ZONE = initialisePriceStation();

    public static Map<TravelZone, Integer> initialisePriceStation() {

        Map<TravelZone, Integer> map = new HashMap();
        map.put(new TravelZone(1, 1), 240);
        map.put(new TravelZone(2, 2), 240);
        map.put(new TravelZone(1, 2), 240);
        map.put(new TravelZone(2, 1), 240);

        map.put(new TravelZone(3, 3), 200);
        map.put(new TravelZone(4, 4), 200);
        map.put(new TravelZone(3, 4), 200);
        map.put(new TravelZone(4, 3), 200);

        map.put(new TravelZone(3, 1), 280);
        map.put(new TravelZone(3, 2), 280);

        map.put(new TravelZone(4, 1), 300);
        map.put(new TravelZone(4, 2), 300);

        map.put(new TravelZone(1, 3), 280);
        map.put(new TravelZone(2, 3), 280);

        map.put(new TravelZone(1, 4), 300);
        map.put(new TravelZone(2, 4), 300);
        return Collections.unmodifiableMap(map);

    }
}
