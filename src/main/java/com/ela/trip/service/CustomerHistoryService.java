package com.ela.trip.service;

import com.ela.trip.constants.PriceList;
import com.ela.trip.constants.ZoneList;
import com.ela.trip.domain.*;
import com.ela.trip.utils.JsonFileParser;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class CustomerHistoryService {

    /**
     * This is method process a and parse the entered file in order to generate an output file that contains customer histories
     * @param input
     * @param output
     */
    public void process(String input, String output) {
        String inputFilePath = Paths.get(input).toString();
        String outputFilePath = Paths.get(output).toString();
        List<CustomerSummary> summaries = new ArrayList<>();
        Optional<CustomerTap> optionalCustomerTap = JsonFileParser.parseInputFile(inputFilePath);
        List<Tap> taps = new ArrayList<>();
        List<Tap> sortedList = new ArrayList<>();
        if (optionalCustomerTap.isPresent()) {
            taps = optionalCustomerTap.get().getTaps();

            Map<Integer, List<Tap>> tapsMap = groupTapByCustomer(taps);
            for (Map.Entry<Integer, List<Tap>> entry : tapsMap.entrySet()) {
                generateCustomerTripSummary(summaries, entry);
            }

            CustomerSummaryList summaryList = new CustomerSummaryList();
            summaryList.setCustomerSummaries(summaries);
            JsonFileParser.generateOutputFile(summaryList, outputFilePath);
            System.out.println("File generated");

        }
    }

    /**
     * Generate a customer summary from his trip
     *
     * @param summaries
     * @param entry
     */
    private void generateCustomerTripSummary(List<CustomerSummary> summaries, Map.Entry<Integer, List<Tap>> entry) {
        CustomerSummary summary = new CustomerSummary();
        List<Tap> sortedList;
        List<Trip> trips = new ArrayList<>();
        sortedList = entry.getValue().stream().sorted(Comparator.comparingLong(Tap::getUnixTimestamp)).collect(Collectors.toList());
        //Generate the list of trip
        generateCustomerTrips(sortedList, trips);
        trips.stream().peek(trip -> {
            List<Integer> zoneStart = getKeys(trip.getStationStart());
            List<Integer> zoneEnd = getKeys(trip.getStationEnd());
            getPrice(trip, zoneStart, zoneEnd);
        }).collect(Collectors.toList());
        summary.setCustomerId(entry.getKey());
        Integer sum = trips.stream().mapToInt(Trip::getCostInCents).sum();
        summary.setTrips(trips);
        summary.setTotalCostInCents(sum);
        summaries.add(summary);
    }

    /**
     * This method return a trip object which has a low cost
     *
     * @param trip
     * @param fromZones
     * @param toZones
     */
    public void getPrice(Trip trip, List<Integer> fromZones, List<Integer> toZones) {
        //Get all possibilities for trip (Multiple zones)
        List<Trip> possibilities = new ArrayList<>();
        for (Integer from : fromZones) {
            for (Integer to : toZones) {
                Trip t = new Trip(from, to, PriceList.PRICE_BY_ZONE.get(new TravelZone(from, to)));
                possibilities.add(t);
            }
        }
        enrichTripObject(trip, possibilities);
    }

    /**
     * Enrich the Trip object
     *
     * @param trip
     * @param possibilities
     */
    private void enrichTripObject(Trip trip, List<Trip> possibilities) {
        Trip lowCostTrip = getLowCostTrip(possibilities);
        trip.setZoneFrom(lowCostTrip.getZoneFrom());
        trip.setZoneTo(lowCostTrip.getZoneTo());
        trip.setCostInCents(lowCostTrip.getCostInCents());
    }

    /**
     * This method return a trip object which has a low cost
     *
     * @param trips
     * @return
     */
    public Trip getLowCostTrip(List<Trip> trips) {

        return trips.stream().min(Comparator.comparing(Trip::getCostInCents)).get();
    }


    /**
     * This method generates a list of customer trip based on his taps
     *
     * @param taps
     * @param trips
     */
    public void generateCustomerTrips(List<Tap> taps, List<Trip> trips) {
        if (taps.size() % 2 != 0) {
            System.out.println("The number of must be pair");
        }
        for (int i = 0; i < taps.size(); i = i + 2) {
            Trip trip = new Trip(taps.get(i).getStation(), taps.get(i + 1).getStation(), taps.get(i).getUnixTimestamp());
            trips.add(trip);
        }
    }

    /**
     * This method return a list of keys based on value
     *
     * @param value
     * @return
     */
    public List<Integer> getKeys(String value) {
        return ZoneList.STATIONS_BY_ZONE
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().contains(value))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    /**
     * Group Taps by Customer's id
     * Genrate a map which customerId is the key and his list of taps as a value
     *
     * @param taps
     * @return
     */
    public Map<Integer, List<Tap>> groupTapByCustomer(List<Tap> taps) {
        return taps.stream()
                .collect(Collectors.groupingBy(Tap::getCustomerId));
    }
}
