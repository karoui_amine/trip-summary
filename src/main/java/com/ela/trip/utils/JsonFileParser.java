package com.ela.trip.utils;

import com.ela.trip.domain.CustomerSummaryList;
import com.ela.trip.domain.CustomerTap;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class JsonFileParser {

    public static final String OUTPUT_TXT = "output.txt";

    /**
     * Parse a json file to CustomerTap optional object
     *
     * @param input
     * @return
     */
    public static Optional<CustomerTap> parseInputFile(String input) {
        CustomerTap customerTap = null;
        ObjectMapper objectMapper = new ObjectMapper();
        if (input == null || input.isEmpty())
            throw new NullPointerException("input must no be null");
        try {
            customerTap = objectMapper.readValue(new File(input), CustomerTap.class);

            return Optional.of(customerTap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Optional.of(customerTap);
    }

    /**
     * Generate an output file
     *
     * @param customerSummary
     * @param output
     */

    public static void generateOutputFile(CustomerSummaryList customerSummary, String output) {
        ObjectMapper objectMapper = new ObjectMapper();
        if (output == null || output.isEmpty())
            throw new NullPointerException("input must no be null");
        File file = new File(output+File.separator+ OUTPUT_TXT);
        try {
            objectMapper.writeValue(file, customerSummary);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
