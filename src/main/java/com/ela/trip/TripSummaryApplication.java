package com.ela.trip;

import com.ela.trip.service.CustomerHistoryService;

import java.util.Scanner;

public class TripSummaryApplication {

    public static void main(String[] args) {
        CustomerHistoryService service = new CustomerHistoryService();
        String inputFilePath;
        String outputFilePath;
        if (args.length == 2) {
            inputFilePath = args[0];
            outputFilePath = args[1];
            service.process(inputFilePath, outputFilePath);
        } else {
            Scanner input = new Scanner(System.in);
            System.out.println("Please enter the path of your input file (ex : C:\\opt\\input.txt)");
            inputFilePath = input.nextLine();
            System.out.println("Please enter the path of generated file (ex = C:\\opt)");
            outputFilePath = input.nextLine();
            service.process(inputFilePath, outputFilePath);
        }


    }

}
