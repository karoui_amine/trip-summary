package com.ela.trip.domain;

import java.util.Objects;

public class TravelZone {

    private Integer zoneFrom;
    private Integer zoneTo;


    public TravelZone() {
    }

    public TravelZone(Integer zoneFrom, Integer zoneTo) {
        this.zoneFrom = zoneFrom;
        this.zoneTo = zoneTo;
    }

    public Integer getZoneFrom() {
        return zoneFrom;
    }

    public void setZoneFrom(Integer zoneFrom) {
        this.zoneFrom = zoneFrom;
    }

    public Integer getZoneTo() {
        return zoneTo;
    }

    public void setZoneTo(Integer zoneTo) {
        this.zoneTo = zoneTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TravelZone that = (TravelZone) o;
        return zoneFrom.equals(that.zoneFrom) &&
                zoneTo.equals(that.zoneTo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(zoneFrom, zoneTo);
    }
}
