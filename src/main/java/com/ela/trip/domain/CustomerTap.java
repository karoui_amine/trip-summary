package com.ela.trip.domain;

import java.util.List;

public class CustomerTap {

    private List<Tap> taps;

    public List<Tap> getTaps() {
        return taps;
    }

    public void setTaps(List<Tap> taps) {
        this.taps = taps;
    }
}
