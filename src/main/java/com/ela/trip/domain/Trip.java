package com.ela.trip.domain;

import java.io.Serializable;

public class Trip implements Serializable {

    private String stationStart;
    private String stationEnd;
    private Long startedJourneyAt;
    private Integer costInCents;
    private Integer zoneFrom;
    private Integer zoneTo;

    public Trip() {

    }

    public Trip(String stationStart, String stationEnd, Long startedJourneyAt) {
        this.stationStart = stationStart;
        this.stationEnd = stationEnd;
        this.startedJourneyAt = startedJourneyAt;
    }

    public Trip(Integer zoneFrom, Integer zoneTo, Integer costInCents) {
        this.costInCents = costInCents;
        this.zoneFrom = zoneFrom;
        this.zoneTo = zoneTo;
    }

    public String getStationStart() {
        return stationStart;
    }

    public void setStationStart(String stationStart) {
        this.stationStart = stationStart;
    }

    public String getStationEnd() {
        return stationEnd;
    }

    public void setStationEnd(String stationEnd) {
        this.stationEnd = stationEnd;
    }

    public Long getStartedJourneyAt() {
        return startedJourneyAt;
    }

    public void setStartedJourneyAt(Long startedJourneyAt) {
        this.startedJourneyAt = startedJourneyAt;
    }

    public Integer getCostInCents() {
        return costInCents;
    }

    public void setCostInCents(Integer costInCents) {
        this.costInCents = costInCents;
    }

    public Integer getZoneFrom() {
        return zoneFrom;
    }

    public void setZoneFrom(Integer zoneFrom) {
        this.zoneFrom = zoneFrom;
    }

    public Integer getZoneTo() {
        return zoneTo;
    }

    public void setZoneTo(Integer zoneTo) {
        this.zoneTo = zoneTo;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "stationStart='" + stationStart + '\'' +
                ", stationEnd='" + stationEnd + '\'' +
                ", startedJourneyAt=" + startedJourneyAt +
                ", costInCents=" + costInCents +
                ", zoneFrom=" + zoneFrom +
                ", zoneTo=" + zoneTo +
                '}';
    }
}
