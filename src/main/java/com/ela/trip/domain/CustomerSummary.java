package com.ela.trip.domain;

import java.io.Serializable;
import java.util.List;

public class CustomerSummary implements Serializable {

    private Integer customerId;
    private Integer totalCostInCents;
    private List<Trip> trips;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getTotalCostInCents() {
        return totalCostInCents;
    }

    public void setTotalCostInCents(Integer totalCostInCents) {
        this.totalCostInCents = totalCostInCents;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }
}
