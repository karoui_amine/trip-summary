package com.ela.trip.domain;

import java.io.Serializable;
import java.util.List;

public class CustomerSummaryList implements Serializable {

    private List<CustomerSummary> customerSummaries;

    public List<CustomerSummary> getCustomerSummaries() {
        return customerSummaries;
    }

    public void setCustomerSummaries(List<CustomerSummary> customerSummaries) {
        this.customerSummaries = customerSummaries;
    }
}
